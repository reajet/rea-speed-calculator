from flask import Flask, render_template, redirect, url_for, send_from_directory, make_response
from flask_talisman import Talisman, GOOGLE_CSP_POLICY

app = Flask(__name__)

# create custom policy with google policy
fonts = GOOGLE_CSP_POLICY['font-src']
scripts = GOOGLE_CSP_POLICY['script-src']

GOOGLE_CSP_POLICY['font-src'] = f"{fonts} *.googleapis.com"
GOOGLE_CSP_POLICY['script-src'] = f"{scripts} *.googletagmanager.com"

CUSTOM_POLICY = {**GOOGLE_CSP_POLICY, **{'img-src': "'self' data: *.google-analytics.com"}}

Talisman(app, content_security_policy=CUSTOM_POLICY)


@app.route('/dod/')
def dod_redirect_page():
    return redirect(url_for('home_page'))


@app.route('/')
@app.route('/<_>/')
def home_page(_=None):
    return redirect("https://speed-calculator.reajetus.com/")


@app.route("/testing")
def testing():
    return render_template("index.html")


@app.route('/service-worker.js')
def service_worker():
    response = make_response(send_from_directory('static', 'service-worker.js'))
    response.headers['Cache-Control'] = 'no-cache'
    return response


@app.route('/manifest.json')
def manifest():
    return send_from_directory('static', 'manifest.json')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
