import binascii
import os
import json


def read_font_file(font_path):

    with open(font_path, 'rb') as f:
        content = f.read()

    # convert binary to hex, then to ascii string
    content = binascii.hexlify(content).decode('ascii')

    # first character number
    first_char_number = int(content[10:12], 16)

    font_code = int(''.join((content[2:4], content[0:2])), 16)

    # get font name
    font_name_data = content[28:68]
    font_name = ''

    for x in range(0, len(font_name_data), 2):
        font_byte = font_name_data[x:x+2]
        if font_byte != '00':
            font_name += font_byte

    # convert to string and strip
    font_name = binascii.unhexlify(font_name).strip().decode('ascii')

    # get the width of the font
    font_width = int(content[14:16], 16)
    font_height = int(content[16:18], 16)
    if font_height > 32:
        return None

    reference_font_code = int(''.join((content[6:8], content[4:6])), 16) + 160
    if reference_font_code > 65535:
        reference_font_code = 0

    if font_code == 0:
        print(f'Font code == 0; {font_path}')
    print(font_path, reference_font_code)

    bytes_per_column = int(content[12:14], 16)
    char_len = font_width * bytes_per_column

    # trim useless data
    content = content[68:-2]

    # determine how many characters
    char_count = int(len(content) / ((char_len + 1) * 2))
    if char_count < 32:
        return None

    list_of_characters = {}

    for x in range(char_count):
        # determine start and end character
        # each font character is (font_width + 1) * 2
        # ( + 1 ) is for the separator/char_width in the string being read
        start_char = (((char_len + 1) * 2) * x) + 2

        # extract the width of the character
        char_width = int(content[start_char - 2:start_char], 16) * bytes_per_column * 2
        end_char = start_char + char_width

        # slice the important characters from string
        char_string = content[start_char:end_char]

        # list hex strings for characters column
        col_list = [int(char_string[i:i+2], 16) for i in range(0, len(char_string), 2)]
        # col_list = [f'0x{char_string[i:i+2]}' for i in range(0, len(char_string), 2)]

        # put it in a dict
        list_of_characters[first_char_number + x] = col_list

    return {font_code: {
        'id': font_code,
        'name': font_name,
        'referenceId': reference_font_code,
        'filename': os.path.basename(font_path),
        'width': font_width,
        'height': font_height,
        'characterCount': char_count,
        'bytesPerColumn': bytes_per_column,
        'characters': list_of_characters,
        'usedAsReference': False,
    }}


cwd = os.getcwd()
fonts_dir = os.path.join(cwd, 'fonts')
fonts = {}
for file in os.listdir(fonts_dir):
    if file.endswith('.szs'):
        # print(f'Reading from file: {file}')
        new_font_data = read_font_file(os.path.join(fonts_dir, file))
        if new_font_data is not None:
            fonts = {**fonts, **new_font_data}

for key, value in dict(fonts).items():
    reference_font_code = value['referenceId']
    if reference_font_code != 0:
        try:
            value['characters'] = {**value['characters'], **fonts[reference_font_code]['characters']}
            fonts[reference_font_code]['usedAsReference'] = True
        except KeyError:
            # don't add if it does not contain a valid reference font code
            # del fonts[key]
            pass

with open(os.path.join(cwd, 'static', 'fonts.js'), 'w') as f:
    json.dump(fonts, f, sort_keys=True)
