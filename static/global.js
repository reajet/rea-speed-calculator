// ServiceWorker is a progressive technology. Ignore unsupported browsers
let newWorker;
if('serviceWorker' in navigator) {
    console.log('CLIENT: service worker registration in progress.');
    navigator.serviceWorker.register('/service-worker.js').then(reg => {
        reg.addEventListener('updatefound', () => {
            console.log('CLIENT: update found');
            newWorker = reg.installing;
            newWorker.addEventListener('statechange', () => {
                if (newWorker.state === "installed") {
                    if (navigator.serviceWorker.controller) {
                        newWorker.postMessage({ action: 'skipWaiting'});
                    }
                }
            })
        });
        console.log('CLIENT: service worker registration complete.');
    }, function() {
        console.log('CLIENT: service worker registration failure.');
    });
    let refreshing;
    navigator.serviceWorker.addEventListener('controllerchange', function() {
        if (refreshing) return;
        window.location.reload();
        refreshing = true;
    });
} else {
    console.log('CLIENT: service worker is not supported.');
}
