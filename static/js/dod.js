// $(document).ready(function () {
    /*
     * Copyright (C) REA Elektronik Inc - All Rights Reserved
     * Unauthorized copying of this file, via any medium is strictly prohibited
     * Proprietary and confidential
     * Author: Jonathan Bluhm; jbluhm@reajetus.com
    */

    let fontsByPrintheadSize = {
        '7': [],
        '16': [],
        '32': []
    };
    for (let fontId in reaFonts) {
        for (let printheadSize in fontsByPrintheadSize) {
            if (printheadSize >= reaFonts[fontId].height) {
                fontsByPrintheadSize[printheadSize].push(fontId);
            }
        }
    }

    const times = n => f => {
        let iter = i => {
            if (i === n) return;
            f(i);
            iter(i + 1)
        };
        return iter(0)
    };

    function roundHalf(num) {
        return Math.round(num * 2) / 2
    }

    function dec2bin(dec) {
        return (dec >>> 0).toString(2).padStart(8, '0')
    }

    function reverse(s) {
        return s.split("").reverse().join("")
    }

    class DodCalculator {
        constructor() {
            this.dotDistanceVerticalMax = 4.5;
            this.svgDotSizeFactor = 1.85;
            this.maxFrequency = 2000;

            this.printheadSelect = document.querySelector('#printheadSelect');
            this.printheadSelect.addEventListener('change', () => this.printheadChange());
            this.printheadSize = this.printheadSelect.value;

            this.printHeightSlider = document.querySelector('#printHeightSlider');
            this.printHeightSlider.addEventListener('input', () => this.printHeightChange());
            this.printHeightPercent = this.printHeightSlider.value / 100;

            this.stretchRatioSlider = document.querySelector('#stretchRatioSlider');
            this.stretchRatioSlider.addEventListener('input', () => this.stretchRatioChange());
            this.stretchRatio = parseInt(this.stretchRatioSlider.value);

            this.stretchRatioInput = document.querySelector('#stretchRatioInput');
            this.stretchRatioInput.addEventListener('change', () => this.stretchRatioInputChange());

            this.lineSpeedSlider = document.querySelector("#lineSpeedSlider");
            this.lineSpeedSlider.addEventListener("input", () => this.lineSpeedSliderChange());
            this.lineSpeed = parseFloat(this.lineSpeedSlider.value);

            this.lineSpeedInput = document.querySelector("#lineSpeedInput");
            this.lineSpeedInput.addEventListener("change", () => this.lineSpeedInputChange());

            this.maxDotSizeDisplay = document.querySelector("#maxDotSize");

            this.fontSelect = document.querySelector('#fontSelect');
            this.fontSelect.addEventListener('change', () => this.fontChange());
            this.fontSelectHtml();
            this.fontSelected = reaFonts[this.fontSelect.value];

            this.scaleSelect = document.querySelector('#scaleSelect');
            this.scaleSelect.addEventListener('change', () => this.scaleChange());
            this.scale = this.scaleSelect.value;

            this.textInput = document.querySelector('#textInput');
            this.textInput.addEventListener('input', () => this.textInputChange());
            this.text = this.textInput.value;

            this.printHeightUSADisplay = document.querySelector('#printHeightUSA');
            this.printHeightMetricDisplay = document.querySelector('#printHeightMetric');
            this.printHeightInput = document.querySelector('#printHeightInput');
            this.printHeightInput.addEventListener('change', () => this.printHeightInputChange());

            this.dpiVerticalDisplay = document.querySelector('#verticalDpi');
            this.dpiHorizontalDisplay = document.querySelector('#horizontalDpi');

            this.dotDistanceVerticalDisplay = document.querySelector('#vdd');
            this.dotDistanceHorizontalDisplay = document.querySelector('#hdd');

            this.maxSpeedMetricDisplay = document.querySelector('#maxSpeedMetric');
            this.maxSpeedUSADisplay = document.querySelector('#maxSpeedUSA');

            this.printheadAngleDisplay = document.querySelector('#printheadAngle');

            this.widthOfDotsDisplay = document.querySelector('#manualDotWidth');
            this.widthOfDotsDisplay.addEventListener('input', () => this.widthOfDotsChange());

            this.shareModalButton = document.querySelector('#shareModal');
            this.shareModalButton.addEventListener('click', () => this.shareModalButtonClick());
            this.shareUrlInput = document.querySelector('#shareUrl');

            this.draw = SVG('reaCanvas').attr(this.canvas);
            this.textDraw = SVG("textCanvas").attr(this.canvas);
            this.printheadGroup = this.draw.group().id('svgPrinthead');
            this.textGroup = this.textDraw.group().id('svgText');

            this.xOffset = 100;

            // this.scrollText = document.querySelector("#scrollText");
            // this.scrollText.addEventListener("input", () => this.checkScrollText());

            this.actualWidthTextMetricDisplay = document.querySelector("#actualWidthTextMetric");
            this.actualWidthTextUSADisplay = document.querySelector("#actualWidthTextUSA");

            this.runner = null;

            this.clipRect = this.textDraw.rect(10000, 5000).x(271).y(-3000);
            let clip = this.draw.clip();
            clip.add(this.clipRect);
            this.textDraw.clipWith(clip);

            this.scaleChange();
            this.drawPrinthead();
            this.drawCharacters();
            // this.checkScrollText();

        }

        get printHeightMetric() {
            if (this.printHeightPercent === 1) {
                return this.printHeightMax.toFixed(1)
            } else {
                return roundHalf(this.printHeightPercent * this.printHeightMax).toFixed(1)
            }
        }

        get printHeightUSA() {
            return (this.printHeightMetric / 25.4).toFixed(1)
        }

        get printHeightMin() {
            return this.printHeightMetric * 0.15;
        }

        get printHeightMax() {
            switch (this.printheadSize) {
                case '7':
                    return 27;
                case '16':
                    return 67.5;
                case '32':
                    return 139.5;
            }
        }

        get dotDistanceVertical() {
            return this.printHeightMetric / (this.printheadSize - 1)
        }

        get dotDistanceHorizontal() {
            return this.dotDistanceVertical * (this.stretchRatio / 100)
        }

        get verticalDpi() {
            return (25.4 / this.dotDistanceVertical).toFixed(2);
        }

        get horizontalDpi() {
            return (25.4 / this.dotDistanceHorizontal).toFixed(2);
        }

        get printheadAngleDisplayValue() {
            return ((Math.asin(this.dotDistanceVertical / this.dotDistanceVerticalMax) * 180) / Math.PI).toFixed(1);
        }

        get printheadAngle() {
            return (Math.acos(this.dotDistanceVertical / this.dotDistanceVerticalMax) * 180) / Math.PI;
        }

        get maxSpeedMetric() {
            const maxSpeed = (((this.dotDistanceHorizontal * this.maxFrequency) * 60) / 1000).toFixed(0);
            this.setLineSpeedMax(maxSpeed);
            return maxSpeed;
        }

        get maxSpeedUSA() {
            return Math.round((this.maxSpeedMetric * 3.28084)).toLocaleString();
        }

        get maxDotSize() {
            // max dot size calculation
            const lineSpeed = this.lineSpeed;
            const slantAngle = this.printheadAngle;
            const nozzleDistance = 0.0045;
            const tClose = 0.00015;
            const tOpenMax = 0.0035;
            const horizontalDistance = (this.stretchRatio / 100) * nozzleDistance * Math.cos(2 * Math.PI / 360 * slantAngle);
            const f = lineSpeed / 60 / horizontalDistance;
            const tTotal = 1 / f;
            const tOpen = tTotal - tClose;
            const maxDotSize = (tOpen / tOpenMax) * 100;
            return Math.min(maxDotSize, 100).toFixed(0);
        }

        get linesOfPrint() {
            const rows = Math.floor(this.printheadSize / this.fontSelected.height);
            const rowsMinus1 = rows - 1;
            const totalNozzles = (this.fontSelected.height * (rows)) + rowsMinus1;

            if (totalNozzles <= this.printheadSize) {
                return rows;
            } else {
                return rowsMinus1;
            }
        }

        get svgDotDistanceVertical() {
            return this.scale * this.dotDistanceVertical;
        }

        get svgDotDistanceHorizontal() {
            return this.scale * this.dotDistanceHorizontal
        }

        get svgDotSize() {
            return this.scale * this.svgDotSizeFactor;
        }

        get shareUrl() {
            if (window.location.hash === '#hr') {
                return false;
            }
            let printheadCodes = {
                '7': 0,
                '16': 1,
                '32': 2,
            };
            let printheadCode = printheadCodes[this.printheadSize];
            let newUrl;
            if (printheadCode === 2 &&
                this.printHeightPercent === 1 &&
                this.stretchRatio === 100 &&
                this.fontSelected.id === 65) {
                newUrl = window.location.origin + '/';
            } else {
                newUrl = `${window.location.origin}/${printheadCode}${parseInt(this.stretchRatioSlider.value).toString(32)}${parseInt(`${this.printHeightSlider.value * 10}`).toString(32)}${this.fontSelected.id.toString(32)}/`
            }
            return newUrl
        }

        fontSelectHtml() {
            const fonts = fontsByPrintheadSize[this.printheadSize];
            let defaultFont;
            if (fonts.includes(this.fontSelect.value)) {
                defaultFont = this.fontSelect.value;
            } else {
                defaultFont = "65";
            }

            this.fontSelect.options.length = 0;

            for (let value of fonts) {
                if (!reaFonts[value].usedAsReference) {
                    this.fontSelect.options.add(new Option(reaFonts[value].name, value, false, value === defaultFont));
                }
            }
        }

        printheadChange() {
            this.printheadSize = this.printheadSelect.value;
            this.fontSelectHtml();
            this.fontChange();
            this.drawPrinthead();
            this.drawCharacters();
            // this.checkScrollText();
            this.printHeightChange();
        }

        textInputChange() {
            if (this.textInput.value === '') {
                this.text = 'REAJET';
            } else {
                this.text = this.textInput.value;
            }
            this.drawCharacters();
            // this.checkScrollText();

            this.actualWidthTextMetricDisplay.innerHTML = this.actualWidthTextMetric;
            this.actualWidthTextUSADisplay.innerHTML = this.actualWidthTextUSA;
        }

        get textWidthDots() {
            return (this.fontSelected.width + 1) * this.text.length;
        }

        get strokeWidth() {
            return this.scale * 0.25;
        }

        get canvas() {
            return {
                height: (181 * this.scale),
            }
        }

        get topDotY() {
            return (this.canvas.height / 2) - ((this.printHeightMetric * this.scale) / 2)
        }

        get svgParams() {
            const printheadParameters = {
                "7": {
                    width: 60,
                    height: 60,
                    radius: 30,
                    nozzlePlateHeight: 50
                },
                "16": {
                    width: 100,
                    height: 100,
                    radius: 50,
                    nozzlePlateHeight: 90
                },
                "32": {
                    width: 73,
                    height: 180,
                    radius: 30,
                    nozzlePlateHeight: 162
                }
            };

            const p = printheadParameters[this.printheadSize];

            return {
                body: {
                    width: (p.width * this.scale) - this.strokeWidth,
                    height: (p.height * this.scale) - this.strokeWidth,
                    radius: (p.radius * this.scale),
                    x: (this.canvas.height / 2) - ((p.width * this.scale) / 2),
                    y: (this.canvas.height / 2) - ((p.height * this.scale) / 2),
                },
                nozzlePlate: {
                    width: (25 * this.scale) - this.strokeWidth,
                    height: (p.nozzlePlateHeight * this.scale) - this.strokeWidth,
                    radius: (5 * this.scale),
                    x: (this.canvas.height / 2) - ((25 * this.scale) / 2),
                    y: (this.canvas.height / 2) - (p.nozzlePlateHeight * this.scale) / 2,
                },
                nozzlePlateChannel: {
                    width: (4 * this.scale) - 1,
                    height: ((this.printHeightMax + 9) * this.scale) - 1,
                    radius: (2 * this.scale),
                    x: (this.canvas.height / 2) - (2 * this.scale),
                    y: (this.canvas.height / 2) - (((this.printHeightMax + 9) * this.scale) / 2),
                }
            }
        }

        get widthOfDots() {
            if (this.widthOfDotsDisplay.value === '') {
                return 0.0;
            } else {
                return (this.dotDistanceHorizontal * (this.widthOfDotsDisplay.value - 1)).toFixed(1);
            }
        }

        get actualWidthTextMetric(){
            return ((this.textWidthDots - 2) * this.dotDistanceHorizontal).toFixed(1);
        }

        get actualWidthTextUSA(){
            return (this.actualWidthTextMetric / 25.4).toFixed(1);
        }

        get animationLineSpeed(){
            let speed;
            if (this.lineSpeed === 1)
            {
                speed = 0.01;
            }
            else
            {
                speed = (this.lineSpeed / 25);
                speed = Math.min(speed, 3);
                speed = Math.max(speed, 0.5);
            }
            return speed;

        }

        // checkScrollText(){
        //     const doScroll = this.scrollText.checked;
        //     if (doScroll)
        //     {
        //         this.startScrollCharacters();
        //     }
        //     else
        //     {
        //         this.stopScrollCharacters();
        //     }
        // }

        startScrollCharacters(){
            if (this.runner !== null)
            {
                this.stopScrollCharacters();
            }

            const xOffset = -(this.textWidthDots * this.svgDotDistanceHorizontal) - 650;
            this.runner = this.textGroup.animate({
                duration: 10000, speed: this.animationLineSpeed
            }).move(window.innerWidth - 300, 0).animate({
                duration: 10000, speed: this.animationLineSpeed
            }).move(xOffset, 0);

            this.runner.reverse();
            this.runner.loop();

            try
            {
                this.runner.speed(this.animationLineSpeed);
            }
            catch (error)
            {
                // do nothing
            }


            return this.runner;
        }

        stopScrollCharacters(){
            if (this.runner === null) return;
            this.runner.animate().move(0, 0);
            this.runner.finish();
        }

        drawPrinthead() {
            this.printheadGroup.clear();

            const p = this.svgParams;

            let bodySvg = this.draw.rect(p.body.width, p.body.height);
            bodySvg.radius(p.body.radius);
            bodySvg.attr({
                fill: '#808080',
                stroke: '#000000',
                'stroke-width': this.strokeWidth,
            });
            bodySvg.x(p.body.x);
            bodySvg.y(p.body.y);
            this.printheadGroup.add(bodySvg);

            let svgNp = this.draw.rect(p.nozzlePlate.width, p.nozzlePlate.height);
            svgNp.radius(p.nozzlePlate.radius);
            svgNp.attr({
                fill: '#cfcfd7',
                stroke: '#383838',
                'stroke-width': this.strokeWidth,
            });
            svgNp.x(p.nozzlePlate.x);
            svgNp.y(p.nozzlePlate.y);
            this.printheadGroup.add(svgNp);

            let svgNpc = this.draw.rect(p.nozzlePlateChannel.width, p.nozzlePlateChannel.height);
            svgNpc.radius(p.nozzlePlateChannel.radius);

            svgNpc.attr({
                fill: '#cfcfd7',
                stroke: '#808080',
                'stroke-width': 1
            });
            svgNpc.x(p.nozzlePlateChannel.x);
            svgNpc.y(p.nozzlePlateChannel.y);
            this.printheadGroup.add(svgNpc);

            const svgDotStartX = (this.canvas.height / 2) - (this.strokeWidth / 2);
            let svgDotStartY = (this.canvas.height / 2) - ((this.printHeightMax * this.scale) / 2);

            times(parseInt(this.printheadSize))(i => {
                const nozzleY = svgDotStartY + (i * (this.dotDistanceVerticalMax * this.scale));
                let nozzle = this.draw.circle(this.svgDotSize);

                nozzle.attr({
                    fill: '#3b3b3b',
                    cy: nozzleY,
                    cx: svgDotStartX,
                });
                this.printheadGroup.add(nozzle);
            });

            // rotate the head so it matches the current angle
            this.printheadGroup.rotate(this.printheadAngle);

            const clipPoint = 90 * this.scale;
            this.clipRect.rotate(this.printheadAngle, clipPoint, clipPoint);
        }

        drawCharacters() {
            this.textGroup.clear();

            let currentColumn = 0;
            [...this.text].forEach(c => {
                const characterCode = c.charCodeAt(0);
                const characterArray = this.fontSelected.characters[characterCode];
                // number of columns for the given character

                for (let i = 0; i < characterArray.length; i = i + this.fontSelected.bytesPerColumn) {
                    let binaryString = '';
                    let dotX =
                        ((this.canvas.height - (45 * this.scale)) + (currentColumn * this.svgDotDistanceHorizontal)) + this.xOffset;
                    let columnData = characterArray.slice(i, i + this.fontSelected.bytesPerColumn);

                    // for each column in the data, convert to binary string and reverse the string
                    columnData.forEach(function (e) {
                        binaryString += reverse(dec2bin(e));
                    });

                    binaryString = binaryString.slice(-this.fontSelected.height);

                    // if the selected printhead can print multiple lines
                    // of text with the selected font, repeat them.
                    if (this.linesOfPrint > 1) {
                        binaryString += '0';
                        binaryString = binaryString.repeat(this.linesOfPrint);
                    }

                    // iterate binary string and if it's a 1, make a dot
                    [...binaryString].forEach((b, idx) => {
                        if (b === '1') {
                            // it is a dot
                            // calculate CY, create dot, add classes, center dot, and add to text group
                            const dotY = this.topDotY + (idx * this.svgDotDistanceVertical);
                            let dot = this.textGroup.circle(this.svgDotSize);
                            dot.addClass(`c${currentColumn} r${idx}`);
                            dot.center(dotX, dotY);
                            // this.textGroup.add(dot);
                        }
                    });
                    currentColumn++;
                }
                currentColumn++;
            });
        }

        positionDotsX() {
            times(this.textWidthDots)(i => {
                const newDotX = ((this.canvas.height - (45 * this.scale)) + (i * this.svgDotDistanceHorizontal)) + this.xOffset;
                let elements = SVG.select(`.c${i}`);
                elements.cx(newDotX);
            });
        }

        printHeightInputChange() {
            let percentage = ((this.printHeightInput.value / this.printHeightMax) * 100);
            this.printHeightSlider.value = percentage.toFixed(1);
            this.printHeightChange();
        }

        printHeightChange() {
            this.printHeightPercent = this.printHeightSlider.value / 100;
            this.printheadGroup.rotate(this.printheadAngle);

            const clipPoint = 90 * this.scale;
            this.clipRect.rotate(this.printheadAngle, clipPoint, clipPoint);

            this.printHeightUSADisplay.innerHTML = this.printHeightUSA;
            this.printHeightMetricDisplay.innerHTML = this.printHeightMetric;

            this.printHeightInput.value = this.printHeightMetric;

            this.dpiVerticalDisplay.setAttribute('data-original-title', this.verticalDpi);
            this.dpiHorizontalDisplay.setAttribute('data-original-title', this.horizontalDpi);

            this.actualWidthTextMetricDisplay.innerHTML = this.actualWidthTextMetric;
            this.actualWidthTextUSADisplay.innerHTML = this.actualWidthTextUSA;

            this.dotDistanceVerticalDisplay.innerHTML = this.dotDistanceVertical.toFixed(2);
            this.dotDistanceHorizontalDisplay.innerHTML = this.dotDistanceHorizontal.toFixed(2);

            this.maxSpeedMetricDisplay.innerHTML = this.maxSpeedMetric;
            this.maxSpeedUSADisplay.innerHTML = this.maxSpeedUSA;

            this.printheadAngleDisplay.innerHTML = this.printheadAngleDisplayValue;
            this.maxDotSizeDisplay.innerHTML = this.maxDotSize;

            this.widthOfDotsChange();

            times(parseInt(this.printheadSize))(i => {
                const newDotY = this.topDotY + (i * this.svgDotDistanceVertical);
                let elements = SVG.select(`.r${i}`);
                elements.cy(newDotY);
            });
            this.positionDotsX();
            // this.checkScrollText();
        }

        lineSpeedSliderChange() {
            this.lineSpeed = parseFloat(this.lineSpeedSlider.value);
            this.lineSpeedInput.value = this.lineSpeedSlider.value;
            this.maxDotSizeDisplay.innerHTML = this.maxDotSize;
            document.querySelector("#lineSpeedMpm").innerHTML = this.lineSpeed.toLocaleString();
            const fpm = Math.round(this.lineSpeed * 3.28084);
            document.querySelector("#lineSpeedFpm").innerHTML = fpm.toLocaleString();

            if (this.runner === null) return;
            if (this.runner.speed() !== this.animationLineSpeed)
            {
                this.runner.speed(this.animationLineSpeed);
            }
        }

        lineSpeedInputChange() {
            this.lineSpeedSlider.value = this.lineSpeedInput.value;
            this.lineSpeedSliderChange();
        }

        setLineSpeedMax(maxValue) {
            this.lineSpeedSlider.max = maxValue;
            this.lineSpeedInput.max = maxValue;
            if (this.lineSpeed >= maxValue) {
                this.lineSpeed = maxValue;
                this.lineSpeedSlider.value = maxValue;
                this.lineSpeedInput.value = maxValue;
                this.lineSpeedSliderChange();
            }
        }

        stretchRatioInputChange() {
            this.stretchRatioSlider.value = this.stretchRatioInput.value;
            this.stretchRatioChange();

        }

        widthOfDotsChange() {
            document.querySelector('#actualWidthMetric').innerHTML = this.widthOfDots;
            document.querySelector('#actualWidthUSA').innerHTML = (this.widthOfDots / 25.4).toFixed(1);
        }

        stretchRatioChange() {
            this.stretchRatio = parseInt(this.stretchRatioSlider.value);
            this.stretchRatioInput.value = this.stretchRatioSlider.value;

            this.maxSpeedMetricDisplay.innerHTML = this.maxSpeedMetric;
            this.maxSpeedUSADisplay.innerHTML = this.maxSpeedUSA;
            this.dpiHorizontalDisplay.setAttribute('data-original-title', this.horizontalDpi);

            this.actualWidthTextMetricDisplay.innerHTML = this.actualWidthTextMetric;
            this.actualWidthTextUSADisplay.innerHTML = this.actualWidthTextUSA;

            this.dotDistanceHorizontalDisplay.innerHTML = this.dotDistanceHorizontal.toFixed(2);
            this.maxDotSizeDisplay.innerHTML = this.maxDotSize;

            this.widthOfDotsChange();

            this.positionDotsX();
        }

        fontChange() {
            this.fontSelected = reaFonts[this.fontSelect.value];
            this.drawCharacters();
            this.actualWidthTextMetricDisplay.innerHTML = this.actualWidthTextMetric;
            this.actualWidthTextUSADisplay.innerHTML = this.actualWidthTextUSA;
            // this.checkScrollText();
        }

        scaleChange() {
            this.scale = this.scaleSelect.value;

            // position elements and set heights.
            const svgDiv = document.querySelector('#reaCanvas');
            svgDiv.style.height = "100%";
            svgDiv.parentElement.style.height = `${this.canvas.height}px`;

            const svgDiv2 = document.querySelector("#textCanvas");
            svgDiv2.style.height = "100%";
            svgDiv2.style.top = `-${this.canvas.height}px`;

            // set x position of clipping rectangle
            this.clipRect.x(90 * this.scale);

            this.drawPrinthead();
            this.drawCharacters();
            // this.checkScrollText();
        }

        shareModalButtonClick() {
            this.shareUrlInput.value = this.shareUrl;
        }

    }
    const inkTable = {
        // TPKD-WS 010
        "1": {
            "ink":{
                "130": [
                    9.893566485,
                    -0.277872809,
                    -21.0559082,
                    2.304873349,
                    0.007162348,
                    -1.140034334,
                    -0.000000295809,
                    21.15468528
                ],
                "180": [
                    34.51891955,
                    0.373176291,
                    -51.71471751,
                    3.228768875,
                    0.02220076,
                    -0.430271871,
                    -0.00000107106,
                    34.62678319
                ],
                "270": [
                    27.93867639,
                    1.789723134,
                    -12.85695745,
                    5.241171215,
                    0.039989046,
                    -0.9317083,
                    -0.00000214272,
                    16.85554968
                ]
            },
            "pMax": 0.8,
            "pMin": 0.4
        },
        // TEP-SW 010
        "2": {
            "ink": {
                "130": [
                    1.200981053,
                    -0.405885958,
                    11.35660804,
                    5.247161805,
                    0.010351384,
                    -2.343358472,
                    -0.000000441117,
                    -105.2603244
                ],
                "180": [
                    28.29716539,
                    -0.660445961,
                    -26.4647616,
                    8.086734008,
                    0.036913167,
                    -0.218699515,
                    -0.00000173733,
                    -117.007545
                ],
                "270": [
                    2.41428214,
                    1.52743501,
                    -72.38930576,
                    20.72538946,
                    0.044095544,
                    -6.975505447,
                    -0.00000265897,
                    70.3175012
                ],
            },
            "pMax": 0.5,
            "pMin": 0.2,
        },
        // TKD-SW 010
        "3": {
            "ink": {
                "130": [
                    7.68428821,
                    -0.350686156,
                    -20.24229163,
                    5.427019876,
                    0.013630258,
                    -3.03338403,
                    -0.000000585732,
                    65.16709963
                ],
                "180": [
                    5.119308612,
                    -1.605074743,
                    94.58552508,
                    9.11152831,
                    0.053310737,
                    -0.704809955,
                    -0.000002352,
                    -391.0472702
                ],
                "270": [
                    -12.51386494,
                    -0.500570581,
                    160.965262,
                    26.08162457,
                    0.067753816,
                    -12.85403626,
                    -0.00000346878,
                    70.46573163
                ]
            },
            "pMax": 0.5,
            "pMin": 0.2,

        }
    }

    function calcInkConsumption(){
        const ink = document.querySelector("#inkSelect").value;
        const p = document.querySelector("#pressureSelect").value;
        const dia = document.querySelector("#nozzleDiameter").value;
        const dotSize = document.querySelector("#dotSize").value;
        const dots = document.querySelector("#dotsPerPrint").value;
        const inkData = inkTable[ink].ink[dia];
        console.log(inkData, inkData[6]);
        const dropVol = inkData[0]
            + (inkData[1] * dotSize)
            + (inkData[2] * p)
            + (inkData[3] * dotSize * p)
            + (inkData[4] * dotSize**2)
            + (inkData[5] * dotSize * p**2)
            + (inkData[6] * dotSize**4)
            + (inkData[7] * p**4);
        const dropsPerLiter = 1_000_000_000 / Math.round(dropVol);
        console.log(dropsPerLiter);
        const printsPerLiter = Math.round(dropsPerLiter/dots);
        const printsPerGallon = Math.round(printsPerLiter * 3.785412);
        document.querySelector("#printsPerLiter").innerHTML = printsPerLiter.toLocaleString();
        document.querySelector("#printsPerGallon").innerHTML = printsPerGallon.toLocaleString();
    }

    function setInkPressure(){
        const ink = document.querySelector("#inkSelect").value;
        const pMin = inkTable[ink].pMin;
        const pMax = inkTable[ink].pMax;
        const p = document.querySelector("#pressureSelect");
        p.min = pMin;
        p.max = pMax;
        if (p.value < pMin)
        {
            p.value = pMin;
        }
        if (p.value > pMax)
        {
            p.value = pMax;
        }
        let inputEvent = new Event("input");
        p.dispatchEvent(inputEvent);
    }

    document.querySelector("#inkSelect").addEventListener("input", setInkPressure);

    document.querySelectorAll("#dod-ink select, #dod-ink input").forEach((element) => {
        element.addEventListener("input", calcInkConsumption);
    });

    document.querySelectorAll("div.slider-display").forEach((element) => {
        const slider = element.querySelector("input[type='range']");
        const input = element.querySelector("input[type='number']");
        const display = element.querySelector(".display");
        input.value = slider.value;
        display.innerHTML = slider.value;
        slider.addEventListener("input", function(){
            input.value = slider.value;
            display.innerHTML = slider.value
        });
        input.addEventListener("change", function(){
            slider.value = input.value;
            let inputEvent = new Event("input");
            slider.dispatchEvent(inputEvent);
        })

        // document.querySelector(`#${element.id}Display`).innerHTML = element.value;
        // element.addEventListener("input", function () {
        //     document.querySelector(`#${element.id}Display`).innerHTML = element.value;
        // });
    })
    calcInkConsumption();

    let testDodCalc = new DodCalculator();
    const url = window.location.pathname;
    if (url === "/" || url === "/testing") {

    } else {
        const printHead = {
            0: "7",
            1: "16",
            2: "32"
        };
        const headSize = printHead[url.slice(1, 2)];
        const stretchRatio = parseInt(url.slice(2, 4), 32);
        const printHeight = parseInt(url.slice(4, 6), 32) / 1000;
        const font = parseInt(url.slice(6, 8), 32);

        testDodCalc.printheadSelect.value = headSize;
        testDodCalc.fontSelect.value = font;
        testDodCalc.printheadChange();

        testDodCalc.stretchRatioInput.value = stretchRatio;
        testDodCalc.stretchRatioInputChange();

        testDodCalc.printHeightInput.value = testDodCalc.printHeightMax * printHeight;
        testDodCalc.printHeightInputChange();

    }

    function changeScale() {
        if (window.innerWidth < 1200) {
            testDodCalc.scaleSelect.value = "1.5";
        } else {
            testDodCalc.scaleSelect.value = "3";
        }
        testDodCalc.scaleChange();
    }
    window.addEventListener('resize', changeScale)
    changeScale();
// });