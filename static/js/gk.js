/*
 * Copyright (C) REA Elektronik Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Author: Jonathan Bluhm; jbluhm@reajetus.com
*/

const headTable = {
    // 4"
    "1": {
        hz: 5000,
    },
    // 2"
    "2": {
        hz: 6000,
    }
}

function gkCalc(){
    const hDpi = parseInt(document.querySelector("#hDpiGk").value);

    // max speed stuff
    const head = document.querySelector("#printheadGk").value;
    const hz = headTable[head].hz;
    const maxFpm = (hz / (hDpi * 12)) * 60;
    const maxMpm = Math.round(maxFpm * 0.3048).toLocaleString();
    document.querySelector("#maxSpeedGkUsa").innerHTML = Math.round(maxFpm).toLocaleString();
    document.querySelector("#maxSpeedGkMetric").innerHTML = maxMpm;

    // max label length stuff
    const pixels = 32768;
    const maxInches = pixels / hDpi;
    document.querySelector("#maxLengthGkUsa").innerHTML = Math.round(maxInches).toLocaleString();
    document.querySelector("#maxLengthGkMetric").innerHTML = Math.round(maxInches / 0.03937).toLocaleString();

}

document.querySelector("#printheadGk").addEventListener("input", gkCalc);
document.querySelector("#hDpiGk").addEventListener("input", gkCalc);