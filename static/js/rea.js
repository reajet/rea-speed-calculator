$(document).ready(function(){
  const clipboard = new ClipboardJS('#copyUrl');
  $('[data-toggle="tooltip"]').tooltip();

  // clipboard stuff
  let copyUrl = document.querySelector('#copyUrl');
  clipboard.on('success', function(e){
    copyUrl.innerHTML = 'Copied';
    // $('#copyUrl').html('Copied!');
  });

  $('#shareUrlModal').on('hidden.bs.modal', function(){
    copyUrl.innerHTML = 'Copy URL';
    // $('#copyUrl').html('Copy URL');
  });

  // open tab depending on hash in url
  let anchor = window.location.hash;
  if (anchor === '') {
    anchor = '#dod';
  } else {
    // hide dod only stuff
    $('.dod-only').fadeOut();
  }
  $(anchor+"-tab").tab('show');

  // update url based on tab selected
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
    let target = e.target.hash;

    if ( target === '#dod' ) {
      // dod page is home page
      target = '#';
      // show dod-only stuff
      $('.dod-only').fadeIn();

    } else{
      // hide dod only stuff
      $('.dod-only').fadeOut();

    }
    history.replaceState(null, null, target);
  });

});
