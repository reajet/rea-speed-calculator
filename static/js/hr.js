/*
 * Copyright (C) REA Elektronik Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Author: Jonathan Bluhm; jbluhm@reajetus.com
*/

const convertTable = {
  "metric": {
    distance: "mm",
    speed: "m/min",
    distanceConvert: 25.4,
    speedConvert: 0.3048,
    mathUnit: 1000,
  },
  "usa": {
    distance: "inches",
    speed: "fpm",
    distanceConvert: 0.0393701,
    speedConvert: 3.28084,
    mathUnit: 12,
  }
}


function hrMaxSpeed(){
  const double = document.querySelector("#doubleSpeed").checked;
  const vDpiSelect = document.querySelector("#vDpi")
  const hDpi = document.querySelector("#hDpi").value;
  const hz = 15000;

  // check if double speed is enabled and if so, set vertical resolution to 300 and disable input
  if (double)
  {
    vDpiSelect.setAttribute("disabled", "disabled");
    vDpiSelect.value = "300";
  }
  else
  {
    vDpiSelect.removeAttribute("disabled");
  }

  let fpm = (hz/(hDpi * 12)) * 60;
  let mpm = fpm * 0.3048;

  // check if double speed is enabled again, and multiply the rounded values by 2.
  if (double)
  {
    fpm = Math.round(fpm) * 2;
    mpm = Math.round(mpm) * 2
  }
  else
  {
    fpm = Math.round(fpm);
    mpm = Math.round(mpm);
  }
  document.querySelector("#maxSpeedF").innerHTML = fpm.toLocaleString();
  document.querySelector("#maxSpeedM").innerHTML = mpm.toLocaleString();
}

document.querySelectorAll("#hr-max-speed input, #hr-max-speed select").forEach((element) => {
  element.addEventListener("input", hrMaxSpeed);
});

/*
Time Calculator stuff.
*/

function changeUnits(){
  // get the new units and the data necessary to convert things.
  const units = document.querySelector("#units").value;
  const dataTable = convertTable[units];

  // update the labels
  document.querySelectorAll(".distanceLabel").forEach((element) => {
    element.innerHTML = dataTable.distance;
  });

  document.querySelectorAll(".speedLabel").forEach((element) => {
    element.innerHTML = dataTable.speed;
  });

  // update the current values using the converting value.
  document.querySelectorAll("#time-calc input.speed").forEach((element) => {
    element.value = (element.value * dataTable.speedConvert).toFixed(2);
  });

  document.querySelectorAll("#time-calc input.distance").forEach((element) => {
    element.value = (element.value * dataTable.distanceConvert).toFixed(2);
  });
}

function calcTime(){
  // get the new units and the data necessary to convert things.
  const units = document.querySelector("#units").value;
  const dataTable = convertTable[units];
  const lineSpeed = parseFloat(document.querySelector("#lineSpeed").value);
  const productLength = parseFloat(document.querySelector("#productLength").value);
  const productDistance = parseFloat(document.querySelector("#productDistance").value);
  const totalLength = productLength + productDistance;
  const pps = ((lineSpeed * dataTable.mathUnit) / 60) / totalLength;
  const timeBetween = ((60/(lineSpeed * dataTable.mathUnit)) * totalLength) * 1000;

  document.querySelector("#pps").innerHTML = pps.toFixed(1);
  document.querySelector("#ppm").innerHTML = (pps * 60).toFixed(1);
  document.querySelector("#time").innerHTML = Math.round(timeBetween).toLocaleString();

}

document.querySelector("#units").addEventListener("input", changeUnits);

document.querySelectorAll("#time-calc input").forEach((element) => {
  element.addEventListener("input", calcTime);
});
