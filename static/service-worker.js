const cacheName = `rea-speed-calc-0.8`;

self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(cacheName).then(cache => {
      return cache.addAll([
        '/',
        '/static/js/pwacompat.min.js',
        '/static/css/bootstrap.min.css',
        '/static/js/jquery.min.js',
        '/static/js/popper.min.js',
        '/static/js/bootstrap.min.js',
        '/static/js/clipboard.min.js',
        '/static/js/svg.min.js',
        '/static/css/rea.css',
        '/static/js/fonts.js',
        '/static/js/hr.js',
        '/static/js/dod.js',
        '/static/js/gk.js',
        '/static/js/rea.js'
      ])
          .then(() => self.skipWaiting());
    })
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(caches.keys().then(function(cacheNames){
    return Promise.all(cacheNames.filter(function(cName){
      if (cName!==cacheName)
        return true;
    }).map(function(cName){
      return caches.delete(cName);
    }));
      })
      // self.clients.claim()
  );
});

self.addEventListener('message', event => {
  if (event.data.action === 'skipWaiting') {
    self.skipWaiting();
  }
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.open(cacheName)
      .then(cache => cache.match(event.request, {ignoreSearch: true}))
      .then(response => {
      return response || fetch(event.request);
    })
  );
});